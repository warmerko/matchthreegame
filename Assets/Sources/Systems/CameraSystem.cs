﻿using Leopotam.Ecs;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class CameraSystem : IEcsInitSystem
    {
        protected readonly GameConfig _config;
        protected readonly GameContext _context;
        protected readonly Camera _camera;

        public CameraSystem(Camera camera, GameConfig config, GameContext context)
        {
            _camera = camera;
            _config = config;
            _context = context;
        }

        public void Init()
        {
            var screenAspectRatio = Screen.width / (float)Screen.height;
            var cameraHalfHeight = Mathf.RoundToInt(Screen.height * .5f);
            var cameraHalfWidth = Mathf.RoundToInt(screenAspectRatio * cameraHalfHeight);

            _camera.orthographicSize = cameraHalfHeight;

            var boxesAspectRatio = _config.Cols / (float)_config.Rows;

            _context.BoxSize = boxesAspectRatio >= screenAspectRatio ? cameraHalfWidth * 2f / _config.Cols : cameraHalfHeight * 2f / _config.Rows;
            _context.HalfBoxSize = _context.BoxSize * .5f;

            _camera.transform.position = new Vector3(cameraHalfWidth, -cameraHalfHeight, -10f);
        }
    }
}
