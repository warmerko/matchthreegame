﻿using Leopotam.Ecs;
using Leopotam.Ecs.Reactive;
using MatchThreeGame.Components;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class InitializeInputSystem : EcsReactiveSystem<EcsFilter<BoxComponent>>
    {
        protected readonly EcsWorld _world;
        protected readonly GameContext _context;
        protected readonly GameConfig _config;

        public InitializeInputSystem(EcsWorld world, GameContext context, GameConfig config)
        {
            _world = world;
            _context = context;
            _config = config;
        }

        protected override EcsReactiveType GetReactiveType()
        {
            return EcsReactiveType.OnAdded;
        }

        protected override void RunReactive()
        {
            foreach (var entity in this)
            {
                var boxComponent = entity.Get<BoxComponent>();
                boxComponent.BoxView.OnDropCallback = direction =>
                {
                    if (TryGetSecondBoxBoardPosition(boxComponent.BoardPosition, direction, boxComponent.BoxView.SpriteRenderer.color,
                        out var secondBoxBoardPosition))
                    {
                        _world.NewEntityWith<BoxSwapEvent>(out var boxSwapEvent);

                        boxSwapEvent.First = boxComponent.BoardPosition;
                        boxSwapEvent.Second = secondBoxBoardPosition.Value;
                    }
                };
            }
        }

        protected bool TryGetSecondBoxBoardPosition(Vector2Int boardPosition, Vector2Int direction, Color color,
            out Vector2Int? position)
        {
            var col = boardPosition.x + direction.x;
            var row = boardPosition.y + direction.y;

            position = null;

            if (col >= _config.Cols || row >= _config.Rows || col < 0 || row < 0)
                return false;

            if (_context.GameBoard[col, row].Get<BoxComponent>().BoxView.SpriteRenderer.color == color)
                return false;

            position = new Vector2Int(col, row);
            return true;
        }
    }
}
