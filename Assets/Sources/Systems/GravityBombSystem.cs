﻿using Leopotam.Ecs;
using MatchThreeGame.Components;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class GravityBombSystem : IEcsRunSystem
    {
        protected readonly EcsFilter<GravityBombEvent> _gravityBombEvents;

        protected readonly GameConfig _config;
        protected readonly GameContext _context;

        public GravityBombSystem(GameConfig config, GameContext context)
        {
            _config = config;
            _context = context;
        }

        public void Run()
        {
            foreach (var i in _gravityBombEvents)
            {
                Vector2Int gravity;

                do
                {
                    gravity = _config.PossibleGravity[Random.Range(0, _config.PossibleGravity.Length)];
                } while (_context.Gravity == gravity);

                _context.Gravity = gravity;
            }
        }
    }
}
