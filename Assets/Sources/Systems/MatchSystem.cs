﻿using Leopotam.Ecs;
using MatchThreeGame.Components;
using MatchThreeGame.Utils;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class MatchSystem : IEcsRunSystem
    {
        protected readonly EcsFilter<BoxSwapEvent> _swapEvents;
        protected readonly EcsFilter<BoxComponent, MovedTag> _movedBoxes;
        protected readonly EcsWorld _world;

        protected readonly HashSet<Vector2Int> _matches;

        protected readonly List<Vector2Int> _localColMatches;
        protected readonly List<Vector2Int> _localRowMatches;

        protected readonly List<Vector2Int> _checks;

        protected readonly GameContext _context;
        protected readonly GameConfig _config;

        public MatchSystem(GameConfig config, GameContext context)
        {
            _config = config;
            _context = context;

            _matches = new HashSet<Vector2Int>();

            _localColMatches = new List<Vector2Int>();
            _localRowMatches = new List<Vector2Int>();

            _checks = new List<Vector2Int>();
        }

        public void Run()
        {
            if (_context.State != GameContext.GameState.Idle) return;

            foreach (var i in _swapEvents)
            {
                var boxSwapEvent = _swapEvents.Get1[i];

                SwapBoxes(boxSwapEvent.First, boxSwapEvent.Second);

                var isFirstMatched = CheckAndProcessBox(boxSwapEvent.First);
                var isSecondMatched = CheckAndProcessBox(boxSwapEvent.Second);

                if (isFirstMatched == false && isSecondMatched == false)
                    SwapBoxes(boxSwapEvent.Second, boxSwapEvent.First);
            }

            foreach (var i in _movedBoxes)
            {
                _movedBoxes.Entities[i].Unset<MovedTag>();

                if (CheckAndProcessBox(_movedBoxes.Get1[i].BoardPosition)) break;
            }
        }

        protected bool CheckAndProcessBox(Vector2Int p)
        {
            var entity = _context.GetGameBoardEntity(p);
            var isMatched = IsMatched(ref entity);

            foreach (var boardPosition in _matches)
                _context.GetGameBoardEntity(boardPosition).Set<MatchedTag>();

            if (_matches.Count >= _config.BombHit)
            {
                var boxComponent = entity.Get<BoxComponent>();

                boxComponent.BoxView.TextField.text = "Bomb";

                entity.Unset<MatchedTag>();
                entity.Set<GravityBombTag>();
            }

            _matches.Clear();

            return isMatched;
        }

        protected bool IsMatched(ref EcsEntity entity)
        {
            var boxComponent = entity.Get<BoxComponent>();

            _checks.Add(boxComponent.BoardPosition);

            do
            {
                var currentBoardPosition = _checks[0];

                _localColMatches.Add(currentBoardPosition);
                _localRowMatches.Add(currentBoardPosition);

                CheckNeighbors(currentBoardPosition, _localRowMatches, Vector2Int.right, bp => bp.x + 1 < _config.Cols);
                CheckNeighbors(currentBoardPosition, _localRowMatches, Vector2Int.left, bp => bp.x - 1 >= 0);
                CheckNeighbors(currentBoardPosition, _localColMatches, Vector2Int.up, bp => bp.y + 1 < _config.Rows);
                CheckNeighbors(currentBoardPosition, _localColMatches, Vector2Int.down, bp => bp.y - 1 >= 0);

                UpdateChecksAndMatches(_localColMatches, currentBoardPosition);
                UpdateChecksAndMatches(_localRowMatches, currentBoardPosition);

                _localColMatches.Clear();
                _localRowMatches.Clear();

                _checks.RemoveAt(0);
            } while (_checks.Count > 0);

            return _matches.Count >= _config.MatchHit;
        }

        protected void UpdateChecksAndMatches(List<Vector2Int> matches, Vector2Int boardPosition)
        {
            if (matches.Count >= _config.MatchHit)
            {
                foreach (var bp in matches)
                {
                    if (bp != boardPosition && _matches.Contains(bp) == false)
                        _checks.Add(bp);
                    _matches.Add(bp);
                }
            }
        }

        protected void CheckNeighbors(Vector2Int currentBoardPosition, List<Vector2Int> localMatches,
            Vector2Int direction, Predicate<Vector2Int> predicate)
        {
            while (predicate(currentBoardPosition))
            {
                var firstBoxComponent = _context.GetGameBoardEntity(currentBoardPosition).Get<BoxComponent>();

                currentBoardPosition.x += direction.x;
                currentBoardPosition.y += direction.y;

                var secondBoxComponent = _context.GetGameBoardEntity(currentBoardPosition).Get<BoxComponent>();

                if (firstBoxComponent.BoxView.SpriteRenderer.color == secondBoxComponent.BoxView.SpriteRenderer.color)
                    localMatches.Add(currentBoardPosition);
                else break;
            }
        }

        protected void SwapBoxes(Vector2Int first, Vector2Int second)
        {
            var firstEntity = _context.GetGameBoardEntity(first);
            var secondEntity = _context.GetGameBoardEntity(second);

            var firstBoxComponent = firstEntity.Get<BoxComponent>();
            var secondBoxComponent = secondEntity.Get<BoxComponent>();

            _context.GameBoard[firstBoxComponent.BoardPosition.x, firstBoxComponent.BoardPosition.y] = secondEntity;
            _context.GameBoard[secondBoxComponent.BoardPosition.x, secondBoxComponent.BoardPosition.y] = firstEntity;

            var boardPosition = firstBoxComponent.BoardPosition;

            firstBoxComponent.BoardPosition = secondBoxComponent.BoardPosition;
            secondBoxComponent.BoardPosition = boardPosition;

            firstBoxComponent.BoxView.transform.position = CoordinatesHelper.BoardToWorldPosition(firstBoxComponent.BoardPosition, _context.BoxSize, _context.HalfBoxSize);
            secondBoxComponent.BoxView.transform.position = CoordinatesHelper.BoardToWorldPosition(secondBoxComponent.BoardPosition, _context.BoxSize, _context.HalfBoxSize);
        }
    }
}
