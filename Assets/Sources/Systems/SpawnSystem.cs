﻿using Leopotam.Ecs;
using MatchThreeGame.Components;
using MatchThreeGame.Pools;
using MatchThreeGame.Utils;
using UnityEngine;

namespace MatchThreeGame.Systems
{
    public class SpawnSystem : IEcsRunSystem, IEcsInitSystem
    {
        protected readonly GameConfig _config;
        protected readonly GameContext _context;

        protected readonly EcsWorld _world;

        protected readonly BoxesPool _boxesPool;

        protected readonly Camera _camera;

        protected readonly EcsFilter<SpawnBoxEvent> _spawnEvents;

        public SpawnSystem(GameConfig config, GameContext context, BoxesPool boxesPool,
            Camera camera, EcsWorld world)
        {
            _config = config;
            _context = context;

            _boxesPool = boxesPool;

            _camera = camera;

            _world = world;
        }

        public void Init()
        {
            var boxesCount = _config.Rows * _config.Cols;

            _boxesPool.Resize(Mathf.RoundToInt(boxesCount * 1.5f));

            for (var row = 0; row < _config.Rows; ++row)
            {
                for (var col = 0; col < _config.Cols; ++col)
                    CreateBoxAt(col, row, _config.BoxColors[Random.Range(0, _config.BoxColors.Length)]);
            }
        }

        protected EcsEntity CreateBoxAt(int col, int row, Color color)
        {
            var entity = _world.NewEntityWith<BoxComponent, DropComponent>(out var boxComponent, out _);

            boxComponent.BoxView = _boxesPool.Spawn();
            boxComponent.BoardPosition = new Vector2Int(col, row);

            _context.GameBoard[col, row] = entity;

            boxComponent.BoxView.transform.localScale = Vector3.one * 0.9f;
            boxComponent.BoxView.transform.position = CoordinatesHelper.BoardToWorldPosition(new Vector2Int(col, row), _context.BoxSize, _context.HalfBoxSize);
            boxComponent.BoxView.Collider.size = new Vector2(_context.BoxSize, _context.BoxSize);
            boxComponent.BoxView.TextField.text = "";

            var spriteRenderer = boxComponent.BoxView.SpriteRenderer;

            spriteRenderer.color = color;
            spriteRenderer.size = new Vector2(_context.BoxSize, _context.BoxSize);

            return entity;
        }

        public void Run()
        {
            foreach (var i in _spawnEvents)
            {
                var spawnEvent = _spawnEvents.Get1[i];

                var spawnPosition = spawnEvent.BoardPosition;

                if (_context.Gravity.x > 0)
                    spawnPosition.x = _config.Cols;
                else if (_context.Gravity.x < 0)
                    spawnPosition.x = -1;
                else if (_context.Gravity.y > 0)
                    spawnPosition.y = _config.Rows;
                else
                    spawnPosition.y = -1;

                var boardPosition = spawnPosition;
                if (_context.Gravity.x != 0)
                    boardPosition.x -= _context.DestroyedBoxesInRow[spawnEvent.BoardPosition.y] * _context.Gravity.x;
                else
                    boardPosition.y -= _context.DestroyedBoxesInCol[spawnEvent.BoardPosition.x] * _context.Gravity.y;

                --_context.DestroyedBoxesInCol[spawnEvent.BoardPosition.x];
                --_context.DestroyedBoxesInRow[spawnEvent.BoardPosition.y];

                var entity = CreateBoxAt(boardPosition.x, boardPosition.y, _config.BoxColors[Random.Range(0, _config.BoxColors.Length)]);
                var position = CoordinatesHelper.BoardToWorldPosition(spawnPosition, _context.BoxSize, _context.HalfBoxSize);

                entity.Set<MoveTag>();

                var boxComponent = entity.Get<BoxComponent>();
                boxComponent.BoxView.transform.position = position;
            }
        }
    }
}
