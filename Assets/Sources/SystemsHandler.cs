﻿using Leopotam.Ecs;
using MatchThreeGame.Systems;
using UnityEngine;
using Zenject;

namespace MatchThreeGame
{
    public class SystemsHandler : MonoBehaviour
    {
        [Inject]
        protected readonly EcsSystems _systems;
        [Inject]
        protected readonly EcsWorld _world;

        [Inject]
        protected readonly CameraSystem _cameraSystem;
        [Inject]
        protected readonly SpawnSystem _spawnSystem;
        [Inject]
        protected readonly InitializeInputSystem _inputSystem;
        [Inject]
        protected readonly MatchSystem _matchSystem;
        [Inject]
        protected readonly DropSystem _dropSystem;
        [Inject]
        protected readonly DestroySystem _destroySystem;
        [Inject]
        protected readonly MoveSystem _moveSystem;
        [Inject]
        protected readonly GravityBombSystem _gravityBombSystem;

        [Inject]
        protected readonly GameContext _context;

        protected void Awake()
        {
#if UNITY_EDITOR
            Leopotam.Ecs.UnityIntegration.EcsWorldObserver.Create(_world);
            Leopotam.Ecs.UnityIntegration.EcsSystemsObserver.Create(_systems);
#endif

            _systems.Add(_cameraSystem)
                .Add(_inputSystem)
                .Add(_matchSystem)
                .Add(_dropSystem)
                .Add(_destroySystem)
                .Add(_spawnSystem)
                .Add(_moveSystem)
                .Add(_gravityBombSystem);

            _systems.Init();
        }

        protected void Update()
        {
            _systems.Run();
            _world.EndFrame();
        }
    }
}
