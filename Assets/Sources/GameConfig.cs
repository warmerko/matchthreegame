﻿using UnityEngine;

namespace MatchThreeGame
{
    [CreateAssetMenu(fileName = "MatchThreeGame-GameConfig", menuName = "MatchThreeGame/GameConfig")]
    public class GameConfig : ScriptableObject
    {
        [Header("Game board settings")]
        public int Rows = 10;
        public int Cols = 10;

        public float DefaultAnimationTime = 0.4f;

        public Vector2Int[] PossibleGravity = new Vector2Int[]
        {
            new Vector2Int(1, 0),
            new Vector2Int(0, 1),
            new Vector2Int(-1, 0),
            new Vector2Int(0, -1)
        };

        [Header("Prefabs")]
        public GameObject BoxPrefab;

        [Header("Gameplay")]
        public Color[] BoxColors;
        public Vector2Int StartGravity = new Vector2Int(0, 1);
        public int MatchHit = 3;
        public int BombHit = 4;
    }
}