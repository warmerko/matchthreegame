﻿using UnityEngine;

namespace MatchThreeGame.Utils
{
    public static class CoordinatesHelper
    {
        public static Vector3 BoardToWorldPosition(Vector2Int boardPosition, float boxSize, float halfBoxSize)
        {
            return new Vector3(boardPosition.x * boxSize + halfBoxSize, -boardPosition.y * boxSize - halfBoxSize);
        }
    }
}
