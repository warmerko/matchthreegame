using Leopotam.Ecs;
using MatchThreeGame.Pools;
using MatchThreeGame.Systems;
using MatchThreeGame.Views;
using UnityEngine;
using Zenject;

namespace MatchThreeGame
{
    public class StartupInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<GameConfig>()
                .FromScriptableObjectResource("MatchThreeGame-GameConfig")
                .AsSingle();

            Container.Bind<GameContext>().AsSingle();

            Container.BindInstance(Camera.main);

            Container.Bind<EcsWorld>().AsSingle();
            Container.Bind<EcsSystems>().AsSingle();

            Container.Bind<CameraSystem>().AsSingle();
            Container.Bind<SpawnSystem>().AsSingle();
            Container.Bind<InitializeInputSystem>().AsSingle();
            Container.Bind<MatchSystem>().AsSingle();
            Container.Bind<DropSystem>().AsSingle();
            Container.Bind<DestroySystem>().AsSingle();
            Container.Bind<MoveSystem>().AsSingle();
            Container.Bind<GravityBombSystem>().AsSingle();

            Container.BindMemoryPool<BoxView, BoxesPool>()
                .FromComponentInNewPrefabResource("Prefabs/BoxPrefab");

            Container.Bind<SystemsHandler>()
                .FromNewComponentOnNewGameObject()
                .WithGameObjectName("Systems Handler")
                .AsSingle()
                .NonLazy();
        }
    }
}