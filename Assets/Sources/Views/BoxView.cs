﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace MatchThreeGame.Views
{
    public class BoxView : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler
    {
        public delegate void BoxViewDelegate(Vector2Int direction);
        public SpriteRenderer SpriteRenderer { get; set; }
        public BoxCollider2D Collider { get; set; }
        public Text TextField { get; set; }

        public BoxViewDelegate OnDropCallback = delegate { };

        protected Vector3 _startPosition;
        protected int _siblingIndex;

        public void OnBeginDrag(PointerEventData eventData)
        {
            _startPosition = transform.position;

            SpriteRenderer.sortingOrder = 1;
        }

        public void OnDrag(PointerEventData eventData)
        {
            var position = eventData.pressEventCamera.ScreenToWorldPoint(eventData.position);

            position.z = transform.position.z;

            transform.position = position;
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            transform.position = _startPosition;

            SpriteRenderer.sortingOrder = 0;
        }

        public void OnDrop(PointerEventData eventData)
        {
            var position = eventData.pressEventCamera.ScreenToWorldPoint(eventData.position);
            var delta = position - _startPosition;

            var direction = Vector2Int.zero;

            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                direction.x = delta.x > 0 ? 1 : -1;
            else
                direction.y = delta.y > 0 ? -1 : 1;

            OnDropCallback(direction);
        }
    }
}
