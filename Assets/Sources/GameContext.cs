﻿using Leopotam.Ecs;
using UnityEngine;

namespace MatchThreeGame
{
    public class GameContext
    {
        public float BoxSize { get; set; }
        public float HalfBoxSize { get; set; }

        public Vector2Int Gravity { get; set; }

        public EcsEntity[,] GameBoard { get; set; }

        public int[] DestroyedBoxesInRow { get; set; }
        public int[] DestroyedBoxesInCol { get; set; }

        public enum GameState
        {
            Idle,
            Destroy,
            DestroyCompleted,
            Move
        }

        public GameState State { get; set; }

        public EcsEntity GetGameBoardEntity(Vector2Int position)
        {
            return GameBoard[position.x, position.y];
        }

        public GameContext(GameConfig config)
        {
            GameBoard = new EcsEntity[config.Cols, config.Rows];
            Gravity = config.StartGravity;

            DestroyedBoxesInRow = new int[config.Rows];
            DestroyedBoxesInCol = new int[config.Cols];
        }
    }
}
