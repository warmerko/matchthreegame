﻿using Leopotam.Ecs;
using UnityEngine;

namespace MatchThreeGame.Components
{
    public class SpawnBoxEvent : IEcsOneFrame
    {
        public Vector2Int BoardPosition;
    }
}
